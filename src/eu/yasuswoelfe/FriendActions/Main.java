/**
 *     FriendActions
 *     Copyright (C) 2020  René Hemm
 *  
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published
 *     by the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *  
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU Affero General Public License for more details.
 *  
 *      You should have received a copy of the GNU Affero General Public License
 *      along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package eu.yasuswoelfe.FriendActions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	public int hugs = 0;
	public int kisses = 0;
	public int pounces = 0;
	public int licks = 0;
	public int slaps = 0;
	public int pokes = 0;
	public int tickles = 0;
	public int caress = 0;

	private Updater updater;
	public Inventory inv = null;

	@Override
	public void onEnable() {
		
		super.onEnable();
		loadconfig();
		checkconfig();

		boolean update = checkforupdate(false);

		registerEvents(update);
		registerCommands();

	}

	private void loadconfig() {

		System.out.println("[FriendActions] (Re)loading config.yml...");
		this.reloadConfig();
		this.getConfig().options().header(
				"***** Config for FriendActions by YasusWoelfe *****\n" + 
				"\n" + 
				"HELP:\n" + 
				"  settings.auto-update:\n" + 
				"    true - The Plugin will show a message if it is up to date or not\n" + 
				"    false - The Plugin will not look for updates\n" + 
				"  settings.enabled.main:\n" + 
				"    true - The Plugin is enabled and can be used\n" + 
				"    false - The Plugin is disabled and it can't be used\n" + 
				"      If something is wrong in the Config.yml, the Plugin switch this option to false\n" + 
				"  settings.enabled.<command>:\n" + 
				"    true - The Command is enabled and can be used\n" + 
				"    false - The Command is disabled and it can't be used\n" + 
				"  settings.effects.must_have_permission:\n" + 
				"    ALONE - If you have Permission, you get an effect\n" + 
				"    SENDER - If the Command-Sender has the Permission, everybody get an effect\n" + 
				"    ONE - If one of the players has the Permission, everybody get an effect\n" + 
				"    EVERYBODY - Only if all Players have the Permission, everybody get an effect\n" + 
				"    NOBODY - The effects are always enabled\n" + 
				"    NEVER - Effects will be disabled\n" + 
				"  settings.gui.<command>.itemname:\n" + 
				"    Select a name for the item in the GUI\n" + 
				"  settings.gui.<command>.itemlore:\n" + 
				"    Select a description for the item in the GUI\n" + 
				"    The {counts}-Block will be replaced through the plugin\n" + 
				"    You can remove it if you do not want to display the counts\n" + 
				"  settings.gui.disabled.itemname:\n" + 
				"    Select a name for the item which is displayed in the GUI if any Command is disabled\n" + 
				"  settings.gui.disabled.itemlore:\n" + 
				"    Select a description for the item which is displayed in the GUI if any Command is disabled\n" + 
				"  messages.<messagegroup>.<message>:\n" + 
				"    Here you can change the Output of the Plugin.\n" + 
				"    You can use Colorcodes like &d\n" + 
				"    The {<text>}-Blocks will be replaced through the plugin. DO NOT CHANGE THEM\n");
		this.getConfig().options().copyDefaults(true);
		this.getConfig().addDefault("settings.auto-update", true);
		this.getConfig().addDefault("settings.enabled.main", true);
		this.getConfig().addDefault("settings.enabled.hug", true);
		this.getConfig().addDefault("settings.enabled.grouphug", true);
		this.getConfig().addDefault("settings.enabled.hugcount", true);
		this.getConfig().addDefault("settings.enabled.kiss", true);
		this.getConfig().addDefault("settings.enabled.kisscount", true);
		this.getConfig().addDefault("settings.enabled.pounce", true);
		this.getConfig().addDefault("settings.enabled.pouncecount", true);
		this.getConfig().addDefault("settings.enabled.lick", true);
		this.getConfig().addDefault("settings.enabled.lickcount", true);
		this.getConfig().addDefault("settings.enabled.slap", true);
		this.getConfig().addDefault("settings.enabled.slapcount", true);
		this.getConfig().addDefault("settings.enabled.poke", true);
		this.getConfig().addDefault("settings.enabled.pokecount", true);
		this.getConfig().addDefault("settings.enabled.tickle", true);
		this.getConfig().addDefault("settings.enabled.ticklecount", true);
		this.getConfig().addDefault("settings.enabled.caress", true);
		this.getConfig().addDefault("settings.enabled.caresscount", true);
		this.getConfig().addDefault("settings.enabled.gui", true);
		this.getConfig().addDefault("settings.effects.must_have_permission", "ALONE");
		this.getConfig().addDefault("settings.gui.hug.itemname", "&6HUG");
		this.getConfig().addDefault("settings.gui.hug.itemlore", Arrays.asList("Hug the Player next to you", "(max. 2 Blocks)", "Counted {counts} hug(s)"));
		this.getConfig().addDefault("settings.gui.grouphug.itemname", "&dGROUP&6HUG");
		this.getConfig().addDefault("settings.gui.grouphug.itemlore", Arrays.asList("Hug all Players around you", "(max. 2 Blocks)", "Counted {counts} hug(s)"));
		this.getConfig().addDefault("settings.gui.kiss.itemname", "&4KISS");
		this.getConfig().addDefault("settings.gui.kiss.itemlore", Arrays.asList("Kiss the Player next to you", "(max. 2 Blocks)", "Counted {counts} kiss(es)"));
		this.getConfig().addDefault("settings.gui.pounce.itemname", "&8POUNCE");
		this.getConfig().addDefault("settings.gui.pounce.itemlore", Arrays.asList("Pounce the Player next to you", "(max. 2 Blocks)", "Counted {counts} pounce(s)"));
		this.getConfig().addDefault("settings.gui.lick.itemname", "&5LICK");
		this.getConfig().addDefault("settings.gui.lick.itemlore", Arrays.asList("Lick the Player next to you", "(max. 2 Blocks)", "Counted {counts} lick(s)"));
		this.getConfig().addDefault("settings.gui.slap.itemname", "&cSLAP");
		this.getConfig().addDefault("settings.gui.slap.itemlore", Arrays.asList("Give the Player next to you a slap", "(max. 2 Blocks)", "Counted {counts} slap(s)"));
		this.getConfig().addDefault("settings.gui.poke.itemname", "&aPOKE");
		this.getConfig().addDefault("settings.gui.poke.itemlore", Arrays.asList("Poke the Player next to you", "(max. 2 Blocks)", "Counted {counts} poke(s)"));
		this.getConfig().addDefault("settings.gui.tickle.itemname", "&eTICKLE");
		this.getConfig().addDefault("settings.gui.tickle.itemlore", Arrays.asList("Tickles the Player next to you", "(max. 2 Blocks)", "Counted {counts} tickle-attack(s)"));
		this.getConfig().addDefault("settings.gui.caress.itemname", "&bCARESS");
		this.getConfig().addDefault("settings.gui.caress.itemlore", Arrays.asList("Caress the Player next to you", "(max. 2 Blocks)", "Counted {counts} caressing Player(s)"));
		this.getConfig().addDefault("settings.gui.disabled.itemname", "&7DISABLED");
		this.getConfig().addDefault("settings.gui.disabled.itemlore", Arrays.asList("This Action is disabled or", "you are not allowed to use it"));
		this.getConfig().addDefault("messages.disabled", "&e{THING}&d is disabled...");
		this.getConfig().addDefault("messages.hug.hashugged", "&dYou hugged &e{PARTNER}");
		this.getConfig().addDefault("messages.hug.washugged", "&dYou was hugged by &e{STARTER}");
		this.getConfig().addDefault("messages.hug.nopartner", "&dThere is nobody to hug...");
		this.getConfig().addDefault("messages.hug.onlyplayers", "&dOnly Players can hug each other...");
		this.getConfig().addDefault("messages.hug.nopermission", "&dYou're not allowed to hug other players!");
		this.getConfig().addDefault("messages.grouphug.hashugged", "&dYou hugged &e{PARTNER}&d in a group");
		this.getConfig().addDefault("messages.grouphug.washugged", "&dYou was hugged by &e{STARTER}&d in a group");
		this.getConfig().addDefault("messages.grouphug.nopartner", "&dThere are not enough Players for a grouphug...");
		this.getConfig().addDefault("messages.grouphug.onlyplayers", "&dOnly Players can hug each other...");
		this.getConfig().addDefault("messages.grouphug.nopermission", "&dYou're not allowed to hug other players in a group!");
		this.getConfig().addDefault("messages.kiss.haskissed", "&dYou kissed &e{PARTNER}");
		this.getConfig().addDefault("messages.kiss.waskissed", "&dYou was kissed by &e{STARTER}");
		this.getConfig().addDefault("messages.kiss.nopartner", "&dThere is nobody to kiss...");
		this.getConfig().addDefault("messages.kiss.onlyplayers", "&dOnly Players can kiss each other...");
		this.getConfig().addDefault("messages.kiss.nopermission", "&dYou're not allowed to kiss other players!");
		this.getConfig().addDefault("messages.pounce.haspounced", "&dYou pounced &e{PARTNER}");
		this.getConfig().addDefault("messages.pounce.waspounced", "&dYou was pounced by &e{STARTER}");
		this.getConfig().addDefault("messages.pounce.nopartner", "&dThere is nobody to pounce...");
		this.getConfig().addDefault("messages.pounce.onlyplayers", "&dOnly Players can pounce each other...");
		this.getConfig().addDefault("messages.pounce.nopermission", "&dYou're not allowed to pounce other players!");
		this.getConfig().addDefault("messages.lick.haslicked", "&dYou licked &e{PARTNER}");
		this.getConfig().addDefault("messages.lick.waslicked", "&dYou was licked by &e{STARTER}");
		this.getConfig().addDefault("messages.lick.nopartner", "&dThere is nobody to lick...");
		this.getConfig().addDefault("messages.lick.onlyplayers", "&dOnly Players can lick each other...");
		this.getConfig().addDefault("messages.lick.nopermission", "&dYou're not allowed to lick other players!");
		this.getConfig().addDefault("messages.slap.hasslaped", "&dYou gave &e{PARTNER} &da slap");
		this.getConfig().addDefault("messages.slap.wasslaped", "&dYou got a slap from &e{STARTER}");
		this.getConfig().addDefault("messages.slap.nopartner", "&dThere is no Player who you can give a slap...");
		this.getConfig().addDefault("messages.slap.onlyplayers", "&dOnly Players can give each other a slap...");
		this.getConfig().addDefault("messages.slap.nopermission", "&dYou're not allowed to give other players a slap!");
		this.getConfig().addDefault("messages.poke.haspoked", "&dYou poked &e{PARTNER}");
		this.getConfig().addDefault("messages.poke.waspoked", "&dYou was poked by &e{STARTER}");
		this.getConfig().addDefault("messages.poke.nopartner", "&dThere is nobody to poke...");
		this.getConfig().addDefault("messages.poke.onlyplayers", "&dOnly Players can poke each other...");
		this.getConfig().addDefault("messages.poke.nopermission", "&dYou're not allowed to poke other players!");
		this.getConfig().addDefault("messages.tickle.hastickled", "&dYou tickled &e{PARTNER}");
		this.getConfig().addDefault("messages.tickle.wastickled", "&dYou was tickled by &e{STARTER}");
		this.getConfig().addDefault("messages.tickle.nopartner", "&dThere is nobody to tickle...");
		this.getConfig().addDefault("messages.tickle.onlyplayers", "&dOnly Players can tickle each other...");
		this.getConfig().addDefault("messages.tickle.nopermission", "&dYou're not allowed to tickle other players!");
		this.getConfig().addDefault("messages.caress.hascaressed", "&dYou caressed &e{PARTNER}");
		this.getConfig().addDefault("messages.caress.wascaressed", "&dYou was caressed by &e{STARTER}");
		this.getConfig().addDefault("messages.caress.nopartner", "&dThere is nobody to caress...");
		this.getConfig().addDefault("messages.caress.onlyplayers", "&dOnly Players can caress each other...");
		this.getConfig().addDefault("messages.caress.nopermission", "&dYou're not allowed to caress other players!");
		this.getConfig().addDefault("messages.hugcount.counts", "&dSince the last Restart there were &e{HUGCOUNT}&d people who hugged eachother.");
		this.getConfig().addDefault("messages.hugcount.nopermission", "&dYou're not allowed to count the hugs!");
		this.getConfig().addDefault("messages.kisscount.counts", "&dSince the last Restart there were &e{KISSCOUNT}&d people who kissed eachother.");
		this.getConfig().addDefault("messages.kisscount.nopermission", "&dYou're not allowed to count the kisses!");
		this.getConfig().addDefault("messages.pouncecount.counts", "&dSince the last Restart there were &e{POUNCECOUNT}&d people who pounced eachother.");
		this.getConfig().addDefault("messages.pouncecount.nopermission", "&dYou're not allowed to count the pounces!");
		this.getConfig().addDefault("messages.lickcount.counts", "&dSince the last Restart there were &e{LICKCOUNT}&d people who licked eachother.");
		this.getConfig().addDefault("messages.lickcount.nopermission", "&dYou're not allowed to count the licks!");
		this.getConfig().addDefault("messages.slapcount.counts", "&dSince the last Restart there were &e{SLAPCOUNT}&d people who has given eachother a slap.");
		this.getConfig().addDefault("messages.slapcount.nopermission", "&dYou're not allowed to count the slaps!");
		this.getConfig().addDefault("messages.pokecount.counts", "&dSince the last Restart there were &e{POKECOUNT}&d people who poked eachother.");
		this.getConfig().addDefault("messages.pokecount.nopermission", "&dYou're not allowed to count the pokes!");
		this.getConfig().addDefault("messages.ticklecount.counts", "&dSince the last Restart there were &e{TICKLECOUNT}&d people who tickled eachother.");
		this.getConfig().addDefault("messages.ticklecount.nopermission", "&dYou're not allowed to count the tickles!");
		this.getConfig().addDefault("messages.caresscount.counts", "&dSince the last Restart there were &e{CARESSCOUNT}&d people who caressed eachother.");
		this.getConfig().addDefault("messages.caresscount.nopermission", "&dYou're not allowed to count the strockes!");
		this.getConfig().addDefault("messages.gui.onlyplayers", "&dOnly Players can open the GUI!");
		this.getConfig().addDefault("messages.gui.nopermission", "&dYou're not allowed to open the GUI!");
		this.getConfig().addDefault("messages.gui.disableditem", "&dThis Action is disabled or you are not allowed to use it!");
		this.getConfig().addDefault("messages.update", "&dThere is a new version of FriendActions! You can download it at {LINK}");
		this.saveConfig();

		System.out.println("[FriendActions] Succesfully (re)loaded config.yml...");
	}

	private void checkconfig() {
		System.out.println("[FriendActions] Checking config.yml...");

		if (this.getConfig().getBoolean("settings.enabled.main") == false) {
			System.out.println("[FriendActions] FriendActions is disabled in the config.yml");
			System.out.println("[FriendActions] Please check the settings.enabled.main-Node");
			System.out.println("[FriendActions] Disabling FriendActions...");
			System.out.println("[FriendActions] FriendActions is now disabled...");
		} else {
			if ((this.getConfig().getString("settings.effects.must_have_permission").trim().equalsIgnoreCase("ALONE")) || (this.getConfig().getString("settings.effects.must_have_permission").trim().equalsIgnoreCase("SENDER")) || (this.getConfig().getString("settings.effects.must_have_permission").trim().equalsIgnoreCase("ONE")) || (this.getConfig().getString("settings.effects.must_have_permission").trim().equalsIgnoreCase("EVERYBODY")) || (this.getConfig().getString("settings.effects.must_have_permission").trim().equalsIgnoreCase("NOBODY")) || (this.getConfig().getString("settings.effects.must_have_permission").trim().equalsIgnoreCase("NEVER"))) {
			} else {
				System.out.println("[FriendActions] Invalid config.yml");
				System.out.println("[FriendActions] Please check the settings.effects.must_have_permission-Node");
				System.out.println("[FriendActions] Disabling FriendActions...");
				this.getConfig().set("settings.enabled.main", false);
				this.saveConfig();
				this.reloadConfig();
				System.out.println("[FriendActions] FriendActions is now disabled...");
			}
		}

		System.out.println("[FriendActions] Succesfully checked config.yml...");
	}

	private boolean checkforupdate(boolean update) {
		if (this.getConfig().getBoolean("settings.auto-update") == true) {

			updater = new Updater(this, 102281, this.getFile(), Updater.UpdateType.NO_DOWNLOAD, false);

			Updater.UpdateResult result = updater.getResult();
			switch (result) {
			case SUCCESS:
				System.out.println("[FriendActions] Update ready! Please restart your server...");
				update = true;
				break;
			case NO_UPDATE:
				System.out.println("[FriendActions] You're up to date!");
				break;
			case DISABLED:
				System.out.println("[FriendActions] The Updater is disabled :(");
				break;
			case FAIL_DOWNLOAD:
				System.out.println("[FriendActions] Can't download the actual version of the plugin :(");
				update = true;
				break;
			case FAIL_DBO:
				System.out.println("[FriendActions] Can't connect to the BukkitDevAPI...");
				System.out.println("[FriendActions] Maybe you have no connection or the server is not available...");
				break;
			case FAIL_NOVERSION:
				System.out.println("[FriendActions] My Developer did a mistake...");
				System.out.println("[FriendActions] The name is incorrect.");
				break;
			case FAIL_BADID:
				System.out.println("[FriendActions] My Developer did a mistake...");
				System.out.println("[FriendActions] The ID is incorrect.");
				break;
			case FAIL_APIKEY:
				System.out.println("[FriendActions] The API has a problem with the API-Key...");
				break;
			case UPDATE_AVAILABLE:
				System.out.println("[FriendActions] Update found...");
				System.out.println("[FriendActions] You can download it at https://dev.bukkit.org/projects/friend-actions");
				update = true;
				break;
			}
		}
		return update;
	}

	private void registerEvents(boolean update) {
		new Event_Listener(this, update);
	}

	private void registerCommands() {
		SimpleCommands sc = new SimpleCommands(this);
		GUI ui = new GUI(this);

		this.getServer().getPluginCommand("hug").setExecutor(sc);
		this.getServer().getPluginCommand("grouphug").setExecutor(sc);
		this.getServer().getPluginCommand("hugcount").setExecutor(sc);
		this.getServer().getPluginCommand("kiss").setExecutor(sc);
		this.getServer().getPluginCommand("kisscount").setExecutor(sc);
		this.getServer().getPluginCommand("pounce").setExecutor(sc);
		this.getServer().getPluginCommand("pouncecount").setExecutor(sc);
		this.getServer().getPluginCommand("lick").setExecutor(sc);
		this.getServer().getPluginCommand("lickcount").setExecutor(sc);
		this.getServer().getPluginCommand("slap").setExecutor(sc);
		this.getServer().getPluginCommand("slapcount").setExecutor(sc);
		this.getServer().getPluginCommand("poke").setExecutor(sc);
		this.getServer().getPluginCommand("pokecount").setExecutor(sc);
		this.getServer().getPluginCommand("tickle").setExecutor(sc);
		this.getServer().getPluginCommand("ticklecount").setExecutor(sc);
		this.getServer().getPluginCommand("caress").setExecutor(sc);
		this.getServer().getPluginCommand("caresscount").setExecutor(sc);

		this.getServer().getPluginCommand("friendactions").setExecutor(ui);
		this.getServer().getPluginCommand("friendactions").setAliases(Arrays.asList("fa", "fagui", "faopen"));

	}

	public void sendMessageToPlayer(Player player, String message) {
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4\u2665 &f" + message + " &4\u2665"));
	}

	public void sendMessageToPlayer(CommandSender sender, String message) {
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4\u2665 &f" + message + " &4\u2665"));
	}

	public String translateString(String str) {
		return ChatColor.translateAlternateColorCodes('&', str);
	}

	public List<String> translateString(List<?> strs, int count) {
		List<String> list = new ArrayList<String>();

		for (Object line : strs) {
			String str = line.toString();
			list.add(translateString(str.replace("{counts}", Integer.toString(count))));
		}

		return list;
	}

}
