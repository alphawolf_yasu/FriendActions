/**
 *     FriendActions
 *     Copyright (C) 2020  René Hemm
 *  
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published
 *     by the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *  
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU Affero General Public License for more details.
 *  
 *      You should have received a copy of the GNU Affero General Public License
 *      along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package eu.yasuswoelfe.FriendActions;

import java.util.ArrayList;

import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

public class SimpleCommands implements CommandExecutor {

	Main plugin;

	public SimpleCommands(Main plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Boolean state = false;

		if (plugin.getConfig().getBoolean("settings.enabled.main") == false) {
			plugin.sendMessageToPlayer(sender,
					plugin.getConfig().getString("messages.disabled").replace("{THING}", "FriendActions"));
			state = true;
		} else {
			if (command.getName().equalsIgnoreCase("hug")) {
				if (plugin.getConfig().getBoolean("settings.enabled.hug") == true) {
					state = hug(sender);
				} else {
					plugin.sendMessageToPlayer(sender,
							plugin.getConfig().getString("messages.disabled").replace("{THING}", "hug"));
					state = true;
				}
			} else if (command.getName().equalsIgnoreCase("hugcount")) {
				if (plugin.getConfig().getBoolean("settings.enabled.hugcount") == true) {
					state = hugcount(sender);
				} else {
					plugin.sendMessageToPlayer(sender,
							plugin.getConfig().getString("messages.disabled").replace("{THING}", "hugcount"));
					state = true;
				}
			} else if (command.getName().equalsIgnoreCase("grouphug")) {
				if (plugin.getConfig().getBoolean("settings.enabled.grouphug") == true) {
					state = grouphug(sender);
				} else {
					plugin.sendMessageToPlayer(sender,
							plugin.getConfig().getString("messages.disabled").replace("{THING}", "grouphug"));
					state = true;
				}
			} else if (command.getName().equalsIgnoreCase("kiss")) {
				if (plugin.getConfig().getBoolean("settings.enabled.kiss") == true) {
					state = kiss(sender);
				} else {
					plugin.sendMessageToPlayer(sender,
							plugin.getConfig().getString("messages.disabled").replace("{THING}", "kiss"));
					state = true;
				}
			} else if (command.getName().equalsIgnoreCase("kisscount")) {
				if (plugin.getConfig().getBoolean("settings.enabled.kisscount") == true) {
					state = kisscount(sender);
				} else {
					plugin.sendMessageToPlayer(sender,
							plugin.getConfig().getString("messages.disabled").replace("{THING}", "kisscount"));
					state = true;
				}
			} else if (command.getName().equalsIgnoreCase("pounce")) {
				if (plugin.getConfig().getBoolean("settings.enabled.pounce") == true) {
					state = pounce(sender);
				} else {
					plugin.sendMessageToPlayer(sender,
							plugin.getConfig().getString("messages.disabled").replace("{THING}", "pounce"));
					state = true;
				}
			} else if (command.getName().equalsIgnoreCase("pouncecount")) {
				if (plugin.getConfig().getBoolean("settings.enabled.pouncecount") == true) {
					state = pouncecount(sender);
				} else {
					plugin.sendMessageToPlayer(sender,
							plugin.getConfig().getString("messages.disabled").replace("{THING}", "pouncecount"));
					state = true;
				}
			} else if (command.getName().equalsIgnoreCase("lick")) {
				if (plugin.getConfig().getBoolean("settings.enabled.lick") == true) {
					state = lick(sender);
				} else {
					plugin.sendMessageToPlayer(sender,
							plugin.getConfig().getString("messages.disabled").replace("{THING}", "lick"));
					state = true;
				}
			} else if (command.getName().equalsIgnoreCase("lickcount")) {
				if (plugin.getConfig().getBoolean("settings.enabled.lickcount") == true) {
					state = lickcount(sender);
				} else {
					plugin.sendMessageToPlayer(sender,
							plugin.getConfig().getString("messages.disabled").replace("{THING}", "lickcount"));
					state = true;
				}
			} else if (command.getName().equalsIgnoreCase("slap")) {
				if (plugin.getConfig().getBoolean("settings.enabled.slap") == true) {
					state = slap(sender);
				} else {
					plugin.sendMessageToPlayer(sender,
							plugin.getConfig().getString("messages.disabled").replace("{THING}", "slap"));
					state = true;
				}
			} else if (command.getName().equalsIgnoreCase("slapcount")) {
				if (plugin.getConfig().getBoolean("settings.enabled.slapcount") == true) {
					state = slapcount(sender);
				} else {
					plugin.sendMessageToPlayer(sender,
							plugin.getConfig().getString("messages.disabled").replace("{THING}", "slapcount"));
					state = true;
				}
			} else if (command.getName().equalsIgnoreCase("poke")) {
				if (plugin.getConfig().getBoolean("settings.enabled.poke") == true) {
					state = poke(sender);
				} else {
					plugin.sendMessageToPlayer(sender,
							plugin.getConfig().getString("messages.disabled").replace("{THING}", "poke"));
					state = true;
				}
			} else if (command.getName().equalsIgnoreCase("pokecount")) {
				if (plugin.getConfig().getBoolean("settings.enabled.pokecount") == true) {
					state = pokecount(sender);
				} else {
					plugin.sendMessageToPlayer(sender,
							plugin.getConfig().getString("messages.disabled").replace("{THING}", "pokecount"));
					state = true;
				}
			} else if (command.getName().equalsIgnoreCase("tickle")) {
				if (plugin.getConfig().getBoolean("settings.enabled.tickle") == true) {
					state = tickle(sender);
				} else {
					plugin.sendMessageToPlayer(sender,
							plugin.getConfig().getString("messages.disabled").replace("{THING}", "tickle"));
					state = true;
				}
			} else if (command.getName().equalsIgnoreCase("ticklecount")) {
				if (plugin.getConfig().getBoolean("settings.enabled.ticklecount") == true) {
					state = ticklecount(sender);
				} else {
					plugin.sendMessageToPlayer(sender,
							plugin.getConfig().getString("messages.disabled").replace("{THING}", "ticklecount"));
					state = true;
				}
			} else if (command.getName().equalsIgnoreCase("caress")) {
				if (plugin.getConfig().getBoolean("settings.enabled.caress") == true) {
					state = caress(sender);
				} else {
					plugin.sendMessageToPlayer(sender,
							plugin.getConfig().getString("messages.disabled").replace("{THING}", "caress"));
					state = true;
				}
			} else if (command.getName().equalsIgnoreCase("caresscount")) {
				if (plugin.getConfig().getBoolean("settings.enabled.caresscount") == true) {
					state = caresscount(sender);
				} else {
					plugin.sendMessageToPlayer(sender,
							plugin.getConfig().getString("messages.disabled").replace("{THING}", "caresscount"));
					state = true;
				}
			}
		}

		return state;
	}

	private boolean hug(CommandSender sender) {
		if (sender.hasPermission("friendactions.hug.command")) {
			if (sender instanceof Player) {

				Player start = (Player) sender;
				Player partner = getNearestPlayer(start);

				if (partner != null) {

					ParticleEffect.FIREWORKS_SPARK.display(1, 1, 1, 1, 50, start.getLocation(), 20);
					ParticleEffect.FIREWORKS_SPARK.display(1, 1, 1, 1, 50, partner.getLocation(), 20);

					plugin.sendMessageToPlayer(start, plugin.getConfig().getString("messages.hug.hashugged")
							.replace("{PARTNER}", partner.getName()));
					plugin.sendMessageToPlayer(partner, plugin.getConfig().getString("messages.hug.washugged")
							.replace("{STARTER}", start.getName()));

					start.playSound(partner.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 10, 1);
					partner.playSound(start.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 10, 1);

					int permitted = getEffects(start, partner, "friendactions.hug.effect");

					switch (permitted) {
					case 1:
						start.setHealth(start.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
					case 2:
						partner.setHealth(partner.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
					case 3:
						start.setHealth(start.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
						partner.setHealth(partner.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
					}

					plugin.hugs = plugin.hugs + 2;

				} else {
					plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.hug.nopartner"));
				}
			} else {
				plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.hug.onlyplayers"));
			}
		} else {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.hug.nopermission"));
		}
		return true;
	}

	private boolean grouphug(CommandSender sender) {
		if (sender.hasPermission("friendactions.grouphug.command")) {
			if (sender instanceof Player) {

				Player start = (Player) sender;
				ArrayList<Player> partners = getNearPlayers(start);

				if (partners.size() > 1) {

					ParticleEffect.FIREWORKS_SPARK.display(1, 1, 1, 1, 50, start.getLocation(), 20);

					for (Player partner : partners) {
						plugin.sendMessageToPlayer(start, plugin.getConfig().getString("messages.grouphug.hashugged")
								.replace("{PARTNER}", partner.getName()));
						start.playSound(partner.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 10, 1);

						ParticleEffect.FIREWORKS_SPARK.display(1, 1, 1, 1, 50, partner.getLocation(), 20);
						plugin.sendMessageToPlayer(partner, plugin.getConfig().getString("messages.hug.washugged")
								.replace("{STARTER}", start.getName()));
						partner.playSound(start.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 10, 1);
					}

					ArrayList<Player> permitted = getEffects(start, partners, "friendactions.grouphug.effect");

					for (Player player : permitted) {
						player.setHealth(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
					}

					plugin.hugs = plugin.hugs + (partners.size() + 1);
				} else {
					plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.grouphug.nopartner"));
				}
			} else {
				plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.hug.onlyplayers"));
			}
		} else {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.hug.nopermission"));
		}
		return true;
	}

	private boolean hugcount(CommandSender sender) {
		if (sender.hasPermission("friendactions.hugcount")) {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.hugcount.counts")
					.replace("{HUGCOUNT}", Integer.toString(plugin.hugs)));
		} else {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.hugcount.nopermission"));
		}
		return true;
	}

	private boolean kiss(CommandSender sender) {
		if (sender.hasPermission("friendactions.kiss.command")) {
			if (sender instanceof Player) {

				Player start = (Player) sender;
				Player partner = getNearestPlayer(start);

				if (partner != null) {

					ParticleEffect.HEART.display(1, 1, 1, 1, 50, start.getLocation(), 20);
					ParticleEffect.HEART.display(1, 1, 1, 1, 50, partner.getLocation(), 20);

					plugin.sendMessageToPlayer(start, plugin.getConfig().getString("messages.kiss.haskissed")
							.replace("{PARTNER}", partner.getName()));
					plugin.sendMessageToPlayer(partner, plugin.getConfig().getString("messages.kiss.waskissed")
							.replace("{STARTER}", start.getName()));

					start.playSound(partner.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 10, 1);
					partner.playSound(start.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 10, 1);

					int permitted = getEffects(start, partner, "friendactions.kiss.effect");

					switch (permitted) {
					case 1:
						start.setFoodLevel(20);
					case 2:
						partner.setFoodLevel(20);
					case 3:
						start.setFoodLevel(20);
						partner.setFoodLevel(20);
					}
					plugin.kisses = plugin.kisses + 2;
				} else {
					plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.kiss.nopartner"));
				}
			} else {
				plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.kiss.onlyplayers"));
			}
		} else {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.kiss.nopermission"));
		}
		return true;
	}

	private boolean kisscount(CommandSender sender) {
		if (sender.hasPermission("friendactions.kisscount")) {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.kisscount.counts")
					.replace("{KISSCOUNT}", Integer.toString(plugin.kisses)));
		} else {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.kisscount.nopermission"));
		}
		return true;
	}

	private boolean pounce(CommandSender sender) {
		if (sender.hasPermission("friendactions.pounce.command")) {
			if (sender instanceof Player) {

				Player start = (Player) sender;
				Player partner = getNearestPlayer(start);

				if (partner != null) {

					ParticleEffect.CLOUD.display(1, 1, 1, 1, 50, start.getLocation(), 20);
					ParticleEffect.CLOUD.display(1, 1, 1, 1, 50, partner.getLocation(), 20);

					plugin.sendMessageToPlayer(start, plugin.getConfig().getString("messages.pounce.haspounced")
							.replace("{PARTNER}", partner.getName()));
					plugin.sendMessageToPlayer(partner, plugin.getConfig().getString("messages.pounce.waspounced")
							.replace("{STARTER}", start.getName()));

					start.playSound(partner.getLocation(), Sound.BLOCK_SAND_FALL, 10, 1);
					partner.playSound(start.getLocation(), Sound.BLOCK_SAND_FALL, 10, 1);

					int permitted = getEffects(start, partner, "friendactions.pounce.effect");

					switch (permitted) {
					case 1:
						start.addPotionEffect(PotionEffectType.JUMP.createEffect(60, 0));
					case 2:
						partner.addPotionEffect(PotionEffectType.SLOW.createEffect(60, 5));
					case 3:
						start.addPotionEffect(PotionEffectType.JUMP.createEffect(60, 0));
						partner.addPotionEffect(PotionEffectType.SLOW.createEffect(60, 5));
					}

					plugin.pounces = plugin.pounces + 2;

				} else {
					plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.pounce.nopartner"));
				}
			} else {
				plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.pounce.onlyplayers"));
			}
		} else {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.pounce.nopermission"));
		}
		return true;
	}

	private boolean pouncecount(CommandSender sender) {
		if (sender.hasPermission("friendactions.pouncecount")) {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.pouncecount.counts")
					.replace("{POUNCECOUNT}", Integer.toString(plugin.pounces)));
		} else {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.pouncecount.nopermission"));
		}
		return true;
	}

	private boolean lick(CommandSender sender) {
		if (sender.hasPermission("friendactions.lick.command")) {
			if (sender instanceof Player) {

				Player start = (Player) sender;
				Player partner = getNearestPlayer(start);

				if (partner != null) {

					ParticleEffect.NOTE.display(1, 1, 1, 1, 50, start.getLocation(), 20);
					ParticleEffect.NOTE.display(1, 1, 1, 1, 50, partner.getLocation(), 20);

					plugin.sendMessageToPlayer(start, plugin.getConfig().getString("messages.lick.haslicked")
							.replace("{PARTNER}", partner.getName()));
					plugin.sendMessageToPlayer(partner, plugin.getConfig().getString("messages.lick.waslicked")
							.replace("{STARTER}", start.getName()));

					start.playSound(partner.getLocation(), Sound.ENTITY_SLIME_JUMP, 10, 1);
					partner.playSound(start.getLocation(), Sound.ENTITY_SLIME_JUMP, 10, 1);

					int permitted = getEffects(start, partner, "friendactions.lick.effect");

					switch (permitted) {
					case 1:
						start.addPotionEffect(PotionEffectType.SPEED.createEffect(60, 0));
					case 2:
						partner.addPotionEffect(PotionEffectType.CONFUSION.createEffect(600, 0));
					case 3:
						start.addPotionEffect(PotionEffectType.SPEED.createEffect(60, 0));
						partner.addPotionEffect(PotionEffectType.CONFUSION.createEffect(600, 0));
					}

					plugin.licks = plugin.licks + 2;

				} else {
					plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.lick.nopartner"));
				}
			} else {
				plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.lick.onlyplayers"));
			}
		} else {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.lick.nopermission"));
		}
		return true;
	}

	private boolean lickcount(CommandSender sender) {
		if (sender.hasPermission("friendactions.lickcount")) {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.lickcount.counts")
					.replace("{LICKCOUNT}", Integer.toString(plugin.licks)));
		} else {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.lickcount.nopermission"));
		}
		return true;
	}

	private boolean slap(CommandSender sender) {
		if (sender.hasPermission("friendactions.slap.command")) {
			if (sender instanceof Player) {

				Player start = (Player) sender;
				Player partner = getNearestPlayer(start);

				if (partner != null) {

					ParticleEffect.LAVA.display(1, 1, 1, 1, 50, start.getLocation(), 20);
					ParticleEffect.LAVA.display(1, 1, 1, 1, 50, partner.getLocation(), 20);

					plugin.sendMessageToPlayer(start, plugin.getConfig().getString("messages.slap.hasslaped")
							.replace("{PARTNER}", partner.getName()));
					plugin.sendMessageToPlayer(partner, plugin.getConfig().getString("messages.slap.wasslaped")
							.replace("{STARTER}", start.getName()));

					start.playSound(partner.getLocation(), Sound.BLOCK_LEVER_CLICK, 10, 1);
					partner.playSound(start.getLocation(), Sound.BLOCK_LEVER_CLICK, 10, 1);

					int permitted = getEffects(start, partner, "friendactions.slap.effect");

					switch (permitted) {
					case 1:
						start.addPotionEffect(PotionEffectType.SPEED.createEffect(60, 0));
					case 2:
						try {
							partner.damage(3);
							partner.setHealth(partner.getHealth() + 3);
						} catch (Exception e) {
							// ignore
						}
					case 3:
						start.addPotionEffect(PotionEffectType.SPEED.createEffect(60, 0));
						try {
							partner.damage(3);
							partner.setHealth(partner.getHealth() + 3);
						} catch (Exception e) {
							// ignore
						}
					}

					plugin.slaps = plugin.slaps + 2;

				} else {
					plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.slap.nopartner"));
				}
			} else {
				plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.slap.onlyplayers"));
			}
		} else {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.slap.nopermission"));
		}
		return true;
	}

	private boolean slapcount(CommandSender sender) {
		if (sender.hasPermission("friendactions.slapcount")) {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.slapcount.counts")
					.replace("{SLAPCOUNT}", Integer.toString(plugin.slaps)));
		} else {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.slapcount.nopermission"));
		}
		return true;
	}

	private boolean poke(CommandSender sender) {
		if (sender.hasPermission("friendactions.poke.command")) {
			if (sender instanceof Player) {

				Player start = (Player) sender;
				Player partner = getNearestPlayer(start);

				if (partner != null) {

					ParticleEffect.REDSTONE.display(1, 1, 1, 0, 50, start.getLocation(), 20);
					ParticleEffect.REDSTONE.display(1, 1, 1, 0, 50, partner.getLocation(), 20);

					plugin.sendMessageToPlayer(start, plugin.getConfig().getString("messages.poke.haspoked")
							.replace("{PARTNER}", partner.getName()));
					plugin.sendMessageToPlayer(partner, plugin.getConfig().getString("messages.poke.waspoked")
							.replace("{STARTER}", start.getName()));

					start.playSound(partner.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 10, 1);
					partner.playSound(start.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 10, 1);

					int permitted = getEffects(start, partner, "friendactions.poke.effect");

					switch (permitted) {
					case 1:
						start.addPotionEffect(PotionEffectType.JUMP.createEffect(60, 0));
					case 2:
						partner.addPotionEffect(PotionEffectType.JUMP.createEffect(60, 0));
					case 3:
						start.addPotionEffect(PotionEffectType.JUMP.createEffect(60, 0));
						partner.addPotionEffect(PotionEffectType.JUMP.createEffect(60, 0));
					}

					plugin.pokes = plugin.pokes + 2;

				} else {
					plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.poke.nopartner"));
				}
			} else {
				plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.poke.onlyplayers"));
			}
		} else {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.poke.nopermission"));
		}
		return true;
	}

	private boolean pokecount(CommandSender sender) {
		if (sender.hasPermission("friendactions.pokecount")) {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.pokecount.counts")
					.replace("{POKECOUNT}", Integer.toString(plugin.pokes)));
		} else {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.pokecount.nopermission"));
		}
		return true;
	}
	
	private boolean tickle(CommandSender sender) {
		if (sender.hasPermission("friendactions.tickle.command")) {
			if (sender instanceof Player) {

				Player start = (Player) sender;
				Player partner = getNearestPlayer(start);

				if (partner != null) {

					ParticleEffect.SNOWBALL.display(1, 1, 1, 0, 50, start.getLocation(), 20);
					ParticleEffect.SNOWBALL.display(1, 1, 1, 0, 50, partner.getLocation(), 20);

					plugin.sendMessageToPlayer(start, plugin.getConfig().getString("messages.tickle.hastickled")
							.replace("{PARTNER}", partner.getName()));
					plugin.sendMessageToPlayer(partner, plugin.getConfig().getString("messages.tickle.wastickled")
							.replace("{STARTER}", start.getName()));

					start.playSound(partner.getLocation(), Sound.BLOCK_SNOW_FALL, 10, 1);
					partner.playSound(start.getLocation(), Sound.BLOCK_SNOW_FALL, 10, 1);

					int permitted = getEffects(start, partner, "friendactions.tickle.effect");

					switch (permitted) {
					case 1:
						start.addPotionEffect(PotionEffectType.WEAKNESS.createEffect(60, 0));
					case 2:
						partner.addPotionEffect(PotionEffectType.WEAKNESS.createEffect(60, 0));
					case 3:
						start.addPotionEffect(PotionEffectType.WEAKNESS.createEffect(60, 0));
						partner.addPotionEffect(PotionEffectType.WEAKNESS.createEffect(60, 0));
					}

					plugin.tickles = plugin.tickles + 2;

				} else {
					plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.tickle.nopartner"));
				}
			} else {
				plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.tickle.onlyplayers"));
			}
		} else {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.tickle.nopermission"));
		}
		return true;
	}

	private boolean ticklecount(CommandSender sender) {
		if (sender.hasPermission("friendactions.ticklecount")) {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.ticklecount.counts")
					.replace("{TICKLECOUNT}", Integer.toString(plugin.tickles)));
		} else {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.ticklecount.nopermission"));
		}
		return true;
	}
	
	private boolean caress(CommandSender sender) {
		if (sender.hasPermission("friendactions.caress.command")) {
			if (sender instanceof Player) {

				Player start = (Player) sender;
				Player partner = getNearestPlayer(start);

				if (partner != null) {

					ParticleEffect.SMOKE_NORMAL.display(1, 1, 1, 0, 50, start.getLocation(), 20);
					ParticleEffect.SMOKE_NORMAL.display(1, 1, 1, 0, 50, partner.getLocation(), 20);

					plugin.sendMessageToPlayer(start, plugin.getConfig().getString("messages.caress.hascaressed")
							.replace("{PARTNER}", partner.getName()));
					plugin.sendMessageToPlayer(partner, plugin.getConfig().getString("messages.caress.wascaressed")
							.replace("{STARTER}", start.getName()));

					start.playSound(partner.getLocation(), Sound.ENTITY_CAT_PURR, 10, 1);
					partner.playSound(start.getLocation(), Sound.ENTITY_CAT_PURR, 10, 1);

					int permitted = getEffects(start, partner, "friendactions.caress.effect");

					switch (permitted) {
					case 1:
						start.addPotionEffect(PotionEffectType.LUCK.createEffect(60, 0));
					case 2:
						partner.addPotionEffect(PotionEffectType.LUCK.createEffect(60, 0));
					case 3:
						start.addPotionEffect(PotionEffectType.LUCK.createEffect(60, 0));
						partner.addPotionEffect(PotionEffectType.LUCK.createEffect(60, 0));
					}

					plugin.caress = plugin.caress + 2;

				} else {
					plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.caress.nopartner"));
				}
			} else {
				plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.caress.onlyplayers"));
			}
		} else {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.caress.nopermission"));
		}
		return true;
	}

	private boolean caresscount(CommandSender sender) {
		if (sender.hasPermission("friendactions.caresscount")) {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.caresscount.counts")
					.replace("{CARESSCOUNT}", Integer.toString(plugin.caress)));
		} else {
			plugin.sendMessageToPlayer(sender, plugin.getConfig().getString("messages.caresscount.nopermission"));
		}
		return true;
	}

	private ArrayList<Player> getNearPlayers(Player start) {

		ArrayList<Player> targets = new ArrayList<Player>();

		for (Entity entity : start.getNearbyEntities(2, 2, 2)) {
			if (entity instanceof Player) {
				targets.add((Player) entity);
			}
		}
		return targets;
	}

	private Player getNearestPlayer(Player start) {

		double distance = Double.MAX_VALUE;
		Player target = null;

		for (Entity entity : start.getNearbyEntities(2, 2, 2)) {
			if (entity instanceof Player) {
				double dis = start.getLocation().distance(entity.getLocation());
				if (distance > dis) {
					distance = dis;
					target = (Player) entity;
				}
			}
		}
		return target;
	}

	private int getEffects(Player sender, Player partner, String perm) {

		/*
		 * settings.effects.must_have_permission: ALONE - If you have
		 * Permission, you get an effect SENDER - If the Command-Sender has the
		 * Permission, everybody get an effect ONE - If one of the players has
		 * the Permission, everybody get an effect EVERYBODY - Only if all
		 * Players have the Permission, everybody get an effect NOBODY - The
		 * effects are always enabled NEVER - Effects will be disabled
		 */

		switch (plugin.getConfig().getString("settings.effects.must_have_permission")) {
		case "ALONE":
			if (sender.hasPermission(perm) && partner.hasPermission(perm)) {
				return 3;
			} else if (sender.hasPermission(perm)) {
				return 1;
			} else if (partner.hasPermission(perm)) {
				return 2;
			} else {
				return 0;
			}
		case "SENDER":
			if (sender.hasPermission(perm)) {
				return 3;
			} else {
				return 0;
			}
		case "ONE":
			if (sender.hasPermission(perm) || partner.hasPermission(perm)) {
				return 3;
			} else {
				return 0;
			}
		case "EVERYBODY":
			if (sender.hasPermission(perm) && partner.hasPermission(perm)) {
				return 3;
			} else {
				return 0;
			}
		case "NOBODY":
			return 3;
		default:
			return 0;
		}
	}

	private ArrayList<Player> getEffects(Player sender, ArrayList<Player> partners, String perm) {

		/*
		 * settings.effects.must_have_permission: ALONE - If you have
		 * Permission, you get an effect SENDER - If the Command-Sender has the
		 * Permission, everybody get an effect ONE - If one of the players has
		 * the Permission, everybody get an effect EVERYBODY - Only if all
		 * Players have the Permission, everybody get an effect NOBODY - The
		 * effects are always enabled NEVER - Effects will be disabled
		 */

		ArrayList<Player> permitted = new ArrayList<Player>();

		switch (plugin.getConfig().getString("settings.effects.must_have_permission")) {
		case "ALONE":
			if (sender.hasPermission(perm)) {
				permitted.add(sender);
			}

			for (Player pl : partners) {
				if (pl.hasPermission(perm)) {
					permitted.add(pl);
				}
			}
		case "SENDER":
			if (sender.hasPermission(perm)) {
				permitted.add(sender);
				permitted.addAll(partners);
			}
		case "ONE":
			int counto = 0;

			if (sender.hasPermission(perm)) {
				permitted.add(sender);
				permitted.addAll(partners);
			} else {
				for (Player pl : partners) {
					if (pl.hasPermission(perm)) {
						counto = counto + 1;
					}
				}

				if (counto > 0) {
					permitted.add(sender);
					permitted.addAll(partners);
				}
			}
		case "EVERYBODY":
			int counteb = 0;

			if (sender.hasPermission(perm)) {
				for (Player pl : partners) {
					if (pl.hasPermission(perm)) {
						counteb = counteb + 1;
					}
				}

				if (counteb == partners.size()) {
					permitted.add(sender);
					permitted.addAll(partners);
				}
			}
		case "NOBODY":
			permitted.add(sender);
			permitted.addAll(partners);
		}
		return permitted;
	}

}
