/**
 *     FriendActions
 *     Copyright (C) 2020  René Hemm
 *  
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published
 *     by the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *  
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU Affero General Public License for more details.
 *  
 *      You should have received a copy of the GNU Affero General Public License
 *      along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package eu.yasuswoelfe.FriendActions;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Event_Listener implements Listener {

	JavaPlugin plugin;
	boolean update;
	
	public Event_Listener(JavaPlugin plugin, boolean update) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		this.plugin = plugin;
		this.update = update;
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onAdminJoin(PlayerJoinEvent e){
		Player player = e.getPlayer();
		if (player.hasPermission("friendactions.updateinfo") && update == true){
			if (plugin.getConfig().getString("messages.update").contains("{LINK}")) {
				player.sendMessage(translateString(plugin.getConfig().getString("messages.update").replace("{LINK}", "https://dev.bukkit.org/projects/friend-actions")));
			} else if (plugin.getConfig().getString("messages.update").equals("")) {
				player.sendMessage(translateString("&dThere is a new version of FriendActions! You can download it at https://dev.bukkit.org/projects/friend-actions"));
			} else {
				player.sendMessage(translateString(plugin.getConfig().getString("messages.update") + "\nDownloadlink: https://dev.bukkit.org/projects/friend-actions"));
			}
		}
	}
	
	@EventHandler
	public void onInvClick(InventoryClickEvent e){
		Player player = (Player) e.getWhoClicked();
		
		if(e.getView().getTitle().equalsIgnoreCase("§1Friend§eActions")){
			e.setCancelled(true);
			
			player.closeInventory();
			
			if(e.getCurrentItem().getItemMeta().getDisplayName().equals(translateString(plugin.getConfig().getString("settings.gui.hug.itemname")))) {
				player.performCommand("hug");
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals(translateString(plugin.getConfig().getString("settings.gui.grouphug.itemname")))) {
				player.performCommand("grouphug");
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals(translateString(plugin.getConfig().getString("settings.gui.kiss.itemname")))) {
				player.performCommand("kiss");
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals(translateString(plugin.getConfig().getString("settings.gui.pounce.itemname")))) {
				player.performCommand("pounce");
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals(translateString(plugin.getConfig().getString("settings.gui.lick.itemname")))) {
				player.performCommand("lick");
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals(translateString(plugin.getConfig().getString("settings.gui.slap.itemname")))) {
				player.performCommand("slap");
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals(translateString(plugin.getConfig().getString("settings.gui.poke.itemname")))) {
				player.performCommand("poke");
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals(translateString(plugin.getConfig().getString("settings.gui.tickle.itemname")))) {
				player.performCommand("tickle");
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals(translateString(plugin.getConfig().getString("settings.gui.caress.itemname")))) {
				player.performCommand("caress");
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals(translateString(plugin.getConfig().getString("settings.gui.disabled.itemname")))) {
				player.sendMessage(translateString(plugin.getConfig().getString("messages.gui.disableditem")));
			} else if (e.getCurrentItem().getItemMeta().getDisplayName().equals(translateString("&1Friend")) || e.getCurrentItem().getItemMeta().getDisplayName().equals(translateString("&7by &4Alphawolf_Yasu")) || e.getCurrentItem().getItemMeta().getDisplayName().equals(translateString("&eActions"))) {
				player.sendMessage(translateString("&8>> &1&nFriend&e&nActions&r &7by &4&l&nAlphawolf_Yasu&r &8<<"));
			}
		}
	}

	private String translateString(String str){
		str = ChatColor.translateAlternateColorCodes('&', str);
		return str;
	}
}
